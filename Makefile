#!/usr/bin/make

MAIN             := music123

GNATMAKE         := gnatmake
BUILDER_OPTIONS  :=

# Inherit ADAFLAGS, LDFLAGS if set in the environment.
ADAFLAGS         ?= -g -O2 -gnatf -gnatwa -gnaty
LDFLAGS          ?=

GZIP_FLAGS       := --best --name
STRIP_OPTION     :=
MSGFMT_FLAGS     :=

DESTDIR          ?=
prefix           ?= /usr
INSTALL_BIN      := $(DESTDIR)$(prefix)/bin
INSTALL_DOC      := $(DESTDIR)$(prefix)/share/doc/$(MAIN)
INSTALL_ETC      := $(DESTDIR)/etc
INSTALL_EXAMPLES := $(DESTDIR)$(prefix)/share/doc/$(MAIN)/examples
INSTALL_LOCALE   := $(DESTDIR)$(prefix)/share/locale
INSTALL_MAN      := $(DESTDIR)$(prefix)/share/man


# Default target.
.PHONY: build
build:
# build prerequisites will be added later

.PHONY: install
install:: build
# install rules will be added later

.PHONY: clean
clean::
	find . -name "*~" -delete
# clean rules will be added later


###########################################################################
# Documentation, examples and system-wide configuration

install::
	install -D --mode 644 README          $(INSTALL_DOC)/README
	install -D --mode 644 music123rc.conf $(INSTALL_ETC)/music123rc
	install -D --mode 644 wavgzplay.sh.1  $(INSTALL_EXAMPLES)/wavgzplay.sh.1
	install -D --mode 755 wavgzplay.sh    $(INSTALL_EXAMPLES)/wavgzplay.sh

###########################################################################
# Main executable

build:
	$(GNATMAKE) $(BUILDER_OPTIONS) $(MAIN) -cargs $(ADAFLAGS) -largs $(LDFLAGS)

install::
	install $(STRIP_OPTION) -D --mode 755 $(MAIN) $(INSTALL_BIN)/$(MAIN)

clean::
	rm -f $(MAIN) b~$(MAIN)* *.o *.ali

###########################################################################
# Manpage

MANPAGE := $(MAIN).1.gz

build: $(MAIN).1.gz

$(MAIN).1.gz: $(MAIN).1
	gzip $(GZIP_FLAGS) $< --stdout > $@

install::
	install -D --mode 644 $(MAIN).1.gz $(INSTALL_MAN)/man1/$(MAIN).1.gz

clean::
	rm -f $(MAIN).1.gz

###########################################################################
# Manpages translations

MAN_TRANSLATIONS        := $(wildcard $(MAIN).*.1)
MAN_TRANSLATION_LOCALES := $(patsubst $(MAIN).%.1,%,$(MAN_TRANSLATIONS))
MAN_TRANSLATION_TARGETS := $(addsuffix .man,$(MAN_TRANSLATION_LOCALES))

build: $(MAN_TRANSLATION_TARGETS)

$(MAN_TRANSLATION_TARGETS): %.man: $(MAIN).%.1
	gzip $(GZIP_FLAGS) $< --stdout > $@

install::
	for locale in $(MAN_TRANSLATION_LOCALES); do \
          install -D --mode 644 $$locale.man $(INSTALL_MAN)/$$locale/man1/$(MAIN).1.gz; \
        done

clean::
	rm -f $(MAN_TRANSLATION_TARGETS)

###########################################################################
# Gettext

PO_FILES   := $(wildcard po/*.po)
MO_LOCALES := $(patsubst po/%.po,%,$(PO_FILES))
MO_TARGETS := $(addsuffix .mo,$(MO_LOCALES))

build: $(MO_TARGETS)

$(MO_TARGETS): %.mo: po/%.po
	msgfmt $(MSGFMT_FLAGS) $< -o $@

install::
	for locale in $(MO_LOCALES); do \
          install -D --mode 644 $$locale.mo $(INSTALL_LOCALE)/$$locale/LC_MESSAGES/$(MAIN).mo; \
        done

clean::
	rm -f $(MO_TARGETS)
